import { createApp } from 'vue'
import App from './App.vue'

//connecting bootstrap using import from here main.js to App.vue 
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";

createApp(App).mount('#app')
